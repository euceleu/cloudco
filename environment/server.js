const express = require('express')
const app = express()
app.use('/', express.static('frontend'));


// global.fetch = require("node-fetch");

// const fetch = require("node-fetch");

const Sequelize = require('sequelize')

const sequelize = new Sequelize('profile', 'felixmanta', 'p@ss', {
    dialect: "mysql",
    host: "localhost"
})

sequelize.authenticate().then(() => {
    console.log("Connected to database")
}).catch((err) => {
    console.log(err)
    console.log("Unable to connect to database")
})


// const Messages = sequelize.define('messages', {
//     subject: Sequelize.STRING,
//     name: Sequelize.STRING,
//     message: Sequelize.TEXT
// })

app.get('/createdb', (request, response) => {
    sequelize.sync({force:true}).then(() => {
        response.status(200).send('tables created')
    }).catch((err) => {
        console.log(err)
        response.status(200).send('could not create tables')
    })
})



app.listen(8080)
