## Introducere

Lumea sportului prezinta una din cele mai importante si bine reprezentate comunitati ale zilelor noastre. De la fotbal sau basket si pana la hockey sau atletism, interesul catre sport a creeat intotdeauna o audienta numeroasa consistand din persoane dintr-o varietate de background-uri. Asadar, cu un numar urias de suporteri si fani inevitabil vor exista grupuri numeroase de persoane ce sunt interesate si de partea analitică sau statistica a sporturilor. Acesti oameni isi pot alimenta setea de cunoastere analizand numerele corespunzatoare activitatior din sportul lor preferat. Cu acest scop, fanii tind sa verfice diferite platforme care le confera o varietate de informatii pe care le pot pune in lucru in felul dorit.

## Descriere problemă 


Spre deosebire de fotbal care este cel mai popular sport al planetei, basketul are o audienta mai scazuta fata de sportul rege. Aceasta este cauzata de faptul ca fata de fotbal care poate fi considerat un sport mondial, basketul se joaca la nivelul cel mai inalt predominant in SUA, mai exact in NBA. Asadar, cu o raza mai mica de acces a suporterilor la meciurile de top, numarul fanilor internationali desi foarte numeros nu se apropie de cel al simpatizantilor fotbalului. Un numar mai mic de suporteri genereaza si un interes mai scazut in partea statistica a basketului aceasta cerere rezultand in mai putine resurse analitice facilitate publicului. Cu un numar mai mic de participanti la joc pe teren, basketul este un sport care este mult mai usor influentat de jucatorii de elita. Acestia pot deveni favoritii publicului, fanii dorind sa fie la zi cu performantele preferatilor lor. Acest lucru poate fi pus in practica cu ajutorul aplicației web Your Favorite Stats care ofera sansa fanilor sa caute cu usurinta numele performerilor or favoriti avand astfel ocazia sa le studieze statisitici precum media punctelor, a paselor de cos sau a recuperarilor savarsite cu doar cateva click-uri. De asemenea, aplicatia prezinta un mini-trivia care ofera sansa persoanelor ce viziteaza platforma sa isi puna la incercare cunostintele ce privesc lumea sportului, avand in acelasi timp sansa sa si le extinda cu fiecare vizita a site-ului.


## Descriere API

In dezvoltarea aplicatiei au fost folosite 2 API-uri, acestea creand impreuna o functionalitate cursiva a platformei web. Primul API este reprezentat de balldontlie API. Cu un nume inspirat dintr-un celebru citat din basket balldontdie ofera acces gratuit la date si statistici legate de NBA. Acesta nu solicita folosirea emailului sau a unui API key si este un proiect open source. Acesta permite un numar maxim de 60 de requesturi pe minut si contine statistici care dateaza din sezonul 1979 si pana la cel curent. Cu un singur request se pot accesa informatii legate de fiecare jucator ce a jucat in NBA in aceasta durata de timp, de la echipa si pozitia pe care joaca pana la inaltime si greutate. De asemenea se pot solicita date legate de echipe si de parcursul lor sezonier precum si informatii legate de meciuri specifice. Iar nu in ultimul rand, API-ul ofera o varietate uriasa de statistici ale jucatorilor din sezonul regulat de NBA.

Al doilea API folosit este Open Trivia Database care oferă un API JSON complet gratuit pentru utilizare în proiecte de programare. Utilizarea acestui API nu necesită o cheie API, trebuind doar să fie generata adresa URL corespunzatoare. Open Trivia Database genereaza un numar ales de intrebari de diferite dificultati. De asemena, intrebarile pot fi clasificate dupa domenii precum Sport, Stiinta, Mitologie etc. si pot sa aiba patru răspunsuri alternative sau pot fi de tipul adevarat sau fals. 

## Flux de date

Aplicatia web Your Favorite Stats este una de tip single page (SPA) care reprezinta o aplicație web sau un site web care interacționează cu utilizatorul rescriind dinamic pagina web curentă cu date noi de pe serverul web, în loc de metoda implicită a unui browser web care foloseste multiple pagini noi intregi. 

In partea de sus a paginii se gaseste implementrea API-ului Open Trivia Database. Cu ajutorul unui fetch, API-ului ii este indicat sa returneze sub forma unui JSON date corespunzatoare unei singure intrebari de dificultate medie din categoria Sport. Intrebarea in sine este introdusa intr-un paragraf care este insotit de 2 Radio Buttonuri corespunzatoare variantelor de rapsuns ‘Adevarat’ si ‘Fals’. Cu apasarea butonului de Submit al raspunsului, un text care indica raspunsul corect al intrebarii este afisat, acesta provenind de asemenea din JSON-ul folosind anterior.

Cel de-al doilea API, balldontlie API, este folosit in prima instanta in tabelul din josul paginii. Cu ajutorul unui fetch, datele referitoare la cele 30 de echipe din NBA sunt primite in format JSON intr-un response. La incarcarea paginii, un tabel cu 3 capete de tabel este generat, datele din cadrul acestuia fiind generate cu ajutorul unui Array ce parcurge JSON-ul si identifica atribute specifice fiecarui element. 

Deasupra tabelului cu echipe se afla un text box in care se poate introduce numele unui jucator cu scopul de a fi cautat. Cu apasarea butonului ‘Search’, tabelul descris mai sus este inlocuit de unul similar care insa contine de aceasta data informatii legate de jucatori. Acestea sunt primite intr-un reponse printr-un fetch care este instruit sa foloseasca numele introdus in textbox-ul pentru a returna doar jucatori cu nume ce se potrivesc cautarii. Asadar, tabelul este populat cu un numar de informatii corespunzatoare jucatorilor ce se potrivesc actiunii precedente. La finalul tabelului, un buton numit ‘Show Stats’ ofera oportunitatea procesarii unui nou tabel cu statistici specifice jucatorului ales. Prin urmare, tabelul este din nou reconstruit, datele fiind nou primite cu ajutorul unui fetch caruia ii este indicat id-ului jucatorului ales, tabelul prezentand astfel statisiticile specifice jucatorului ales.


## Capturi ecran aplicatie

1. Pagina la accesare 

![Scheme](pics/p1.png)


2. Dupa raspunderea la intrebarea trivia, raspunsul corect este oferit

![Scheme](pics/p2.png)


3. Lista cu toate echipele active din NBA

![Scheme](pics/p3.png)


4. Dupa folosirea search-ului, tabelul cu jucatorii corespunzatori este generat

![Scheme](pics/p4.png)


5. Cu apasarea butonului ‘Show Stats’ din dreptul unui jucator, statisticile acestuia sunt afisate.

![Scheme](pics/p5.png)


6. Dupa analizarea statisticilor, o noua cautare se poate savarsi.

![Scheme](pics/p6.png)



## Referinte

[WebTech Superheroes](https://tutoriale.webtech-superheroes.net/configurare-mediu-de-lucru/c9)

[Amazon Cloud9](https://aws.amazon.com/cloud9/)

[balldontlie](https://www.balldontlie.io/#introduction/)

[Open Trivia Database](https://opentdb.com/)